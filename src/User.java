import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class User {
	
	public PrivateKey privateKey;
	public PublicKey publicKey;
	
	public HashMap<String, TxOut> UTXOs = new HashMap<String, TxOut>();
	
	public User() {
		generateKeyPair();
	}

	public void generateKeyPair() {
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("ECDSA","BC");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
			ECGenParameterSpec ecSpec = new ECGenParameterSpec("prime192v1");
			keyGen.initialize(ecSpec, random);
	        KeyPair keyPair = keyGen.generateKeyPair();
	        privateKey = keyPair.getPrivate();
	        publicKey = keyPair.getPublic();

		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public float getBalance(Blockchain blockchain) {
		float total = 0;	
        for (Map.Entry<String, TxOut> item: blockchain.UTXOs.entrySet()){
        	TxOut UTXO = item.getValue();
            if(UTXO.isMine(publicKey)) {
            	UTXOs.put(UTXO.id,UTXO);
            	total += UTXO.value ; 
            }
        }  
		return total;
	}
	
	public Transaction sendFunds(Blockchain blockchain, PublicKey _recipient, float value ) {
		if(getBalance(blockchain) < value) {
			System.out.println("#Not Enough funds to send transaction. Transaction Discarded.");
			return null;
		}
		ArrayList<TxIn> inputs = new ArrayList<TxIn>();
		
		float total = 0;
		for (Map.Entry<String, TxOut> item: UTXOs.entrySet()){
			TxOut UTXO = item.getValue();
			total += UTXO.value;
			inputs.add(new TxIn(UTXO.id));
			if(total > value) break;
		}
		
		Transaction newTransaction = new Transaction(publicKey, _recipient , value, inputs);
		newTransaction.generateSignature(privateKey);
		
		for(TxIn input: inputs){
			UTXOs.remove(input.transactionOutputId);
		}
		
		return newTransaction;
	}
	
}


