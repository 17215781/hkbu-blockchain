//import java.util.Base64;
import java.security.Security;



public class Main {

	public static void main(String[] args) {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()); //Setup Bouncey castle as a Security Provider

        User userB = new User();
        User userC = new User();

        Blockchain blockchain = new Blockchain();


		System.out.println("Creating and Mining Genesis block... ");

		Block genesisBlock = blockchain.generateGenesisBlock();
		blockchain.addBlock(genesisBlock);


		//testing
		Block block1 = blockchain.generateNextBlock();
		blockchain.addBlock(block1);

        Block block2 = blockchain.generateNextBlock();
        blockchain.addBlock(block2);

        Block block3 = blockchain.generateNextBlock();
        blockchain.addBlock(block2);


        System.out.println("\nUserA is Attempting to send  (40) to UserB...");

		block2.addTransaction(blockchain, blockchain.userA.sendFunds(blockchain, userB.publicKey, 40));
		System.out.println("\nUserA's balance is: " + blockchain.userA.getBalance(blockchain));
		System.out.println("UserB's balance is: " + userB.getBalance(blockchain));

		System.out.println("\nUserA is Attempting to send  (20) to UserC...");
		block2.addTransaction(blockchain, blockchain.userA.sendFunds(blockchain, userC.publicKey, 20));

        System.out.println("\nUserA's balance is: " + blockchain.userA.getBalance(blockchain));
        System.out.println("UserB's balance is: " + userB.getBalance(blockchain));
        System.out.println("UserC's balance is: " + userC.getBalance(blockchain));

        System.out.println("\nUserA is Attempting to send  (30) to UserB again...");
        block3.addTransaction(blockchain, blockchain.userA.sendFunds(blockchain, userB.publicKey, 30));

        System.out.println("\nUserA's balance is: " + blockchain.userA.getBalance(blockchain));
        System.out.println("UserB's balance is: " + userB.getBalance(blockchain));
        System.out.println("UserC's balance is: " + userC.getBalance(blockchain));

        System.out.print(genesisBlock);
        System.out.print(block1);
        System.out.print(block2);
        System.out.print(block3);



        System.out.print("\nSize:" + blockchain.getSize());

        blockchain.isChainValid();

	}

}